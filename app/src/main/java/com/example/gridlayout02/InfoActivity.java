package com.example.gridlayout02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    private TextView infoTitle;
    private ImageView infoImage;
    private TextView infoYear;
    private TextView infoTime;
    private TextView infoSong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        infoTitle = findViewById(R.id.textTitleInfo);
        infoImage = findViewById(R.id.imgInfo);
        infoYear = findViewById(R.id.infoYear);
        infoTime = findViewById(R.id.infoTime);
        infoSong = findViewById(R.id.infoSong);

        Intent i = getIntent();
        String sInfoTitle = i.getStringExtra(MainActivity.INFO_TITLE);
        int iinfoImage = i.getIntExtra(MainActivity.INFO_IMG, 0);
        String sInfoYear = i.getStringExtra(MainActivity.INFO_YEAR);
        String sInfoTime = i.getStringExtra(MainActivity.INFO_TIME);
        String sInfoSong = i.getStringExtra(MainActivity.INFO_SONG);
        infoTitle.setText(sInfoTitle);
        infoImage.setImageResource(iinfoImage);
        infoYear.setText(sInfoYear);
        infoTime.setText(sInfoTime);
        infoSong.setText(sInfoSong);

    }
}