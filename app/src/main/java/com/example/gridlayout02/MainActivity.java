package com.example.gridlayout02;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public static String INFO_TITLE = "com.example.gridlayout02.INFO_TITLE";
    public static String INFO_IMG = "com.example.gridlayout02.INFO_IMG";
    public static String INFO_YEAR = "com.example.gridlayout02.INFO_YEAR";
    public static String INFO_TIME = "com.example.gridlayout02.INFO_TIME";
    public static String INFO_SONG = "com.example.gridlayout02.INFO_SONG";
    private CardView cardView11;
    private CardView cardView12;
    private CardView cardView21;
    private CardView cardView22;
    private CardView cardView31;
    private CardView cardView32;
    private CardView cardView41;
    private CardView cardView42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cardView11 = findViewById(R.id.cardView11);
        cardView12 = findViewById(R.id.cardView12);
        cardView21 = findViewById(R.id.cardView21);
        cardView22 = findViewById(R.id.cardView22);
        cardView31 = findViewById(R.id.cardView31);
        cardView32 = findViewById(R.id.cardView32);
        cardView41 = findViewById(R.id.cardView41);
        cardView42 = findViewById(R.id.cardView42);

        cardView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String infoTitle = getString(R.string.textDisc1);
                int infoImage = R.drawable.disc1;
                String infoYear = "1998";
                String infoTime = "44:11";
                String infoSong = "El 28\n" +
                        "Cuéntame al oído\n" +
                        "Pesadilla\n" +
                        "La estrella y la luna\n" +
                        "Viejo cuento\n" +
                        "Dos cristales\n" +
                        "Lloran piedras\n" +
                        "Qué puedo pedir\n" +
                        "Dile al sol\n" +
                        "El libro\n" +
                        "La carta\n" +
                        "Soñaré";
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(INFO_TITLE, infoTitle);
                intent.putExtra(INFO_IMG, infoImage);
                intent.putExtra(INFO_YEAR, infoYear);
                intent.putExtra(INFO_TIME, infoTime);
                intent.putExtra(INFO_SONG, infoSong);
                startActivity(intent);
            }
        });
        cardView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String infoTitle = getString(R.string.textDisc2);
                int infoImage = R.drawable.disc2;
                String infoYear = "2000";
                String infoTime = "52:52";
                String infoSong = "Cuídate\n" +
                        "Soledad\n" +
                        "París\n" +
                        "La playa\n" +
                        "Pop\n" +
                        "Dicen que dicen\n" +
                        "Mariposa\n" +
                        "La chica del gorro azul\n" +
                        "Tu pelo\n" +
                        "Tantas cosas que contar\n" +
                        "Los amantes del círculo polar\n" +
                        "Desde el puerto\n" +
                        "Tic Tac";
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(INFO_TITLE, infoTitle);
                intent.putExtra(INFO_IMG, infoImage);
                intent.putExtra(INFO_YEAR, infoYear);
                intent.putExtra(INFO_TIME, infoTime);
                intent.putExtra(INFO_SONG, infoSong);
                startActivity(intent);
            }
        });
        cardView21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String infoTitle = getString(R.string.textDisc3);
                int infoImage = R.drawable.disc3;
                String infoYear = "2003";
                String infoTime = "53:48";
                String infoSong = "Puedes contar conmigo\n" +
                        "20 de enero\n" +
                        "Rosas\n" +
                        "Deseos de cosas imposibles\n" +
                        "Geografía\n" +
                        "Un mundo mejor\n" +
                        "Tú y yo\n" +
                        "La esperanza debida\n" +
                        "Vestido azul\n" +
                        "Adiós\n" +
                        "Perdóname\n" +
                        "La paz de tus ojos\n" +
                        "Nadie como tú\n" +
                        "Historia de un sueño\n" +
                        "Bonustrack";
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(INFO_TITLE, infoTitle);
                intent.putExtra(INFO_IMG, infoImage);
                intent.putExtra(INFO_YEAR, infoYear);
                intent.putExtra(INFO_TIME, infoTime);
                intent.putExtra(INFO_SONG, infoSong);
                startActivity(intent);
            }
        });
        cardView22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String infoTitle = getString(R.string.textDisc4);
                int infoImage = R.drawable.disc4;
                String infoYear = "2006";
                String infoTime = "45:56";
                String infoSong = "Noche\n" +
                        "Muñeca de trapo\n" +
                        "Dulce locura\n" +
                        "Perdida\n" +
                        "Vuelve\n" +
                        "Escapar\n" +
                        "Irreversible\n" +
                        "A diez centímetros de ti\n" +
                        "V.O.S.\n" +
                        "Apareces tú\n" +
                        "Manhattan\n" +
                        "Mi vida sin ti\n" +
                        "Cuántos cuentos cuento";
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(INFO_TITLE, infoTitle);
                intent.putExtra(INFO_IMG, infoImage);
                intent.putExtra(INFO_YEAR, infoYear);
                intent.putExtra(INFO_TIME, infoTime);
                intent.putExtra(INFO_SONG, infoSong);
                startActivity(intent);
            }
        });
        cardView31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String infoTitle = getString(R.string.textDisc5);
                int infoImage = R.drawable.disc5;
                String infoYear = "2008";
                String infoTime = "49:01";
                String infoSong = "El último vals\n" +
                        "Inmortal\n" +
                        "Jueves\n" +
                        "Más\n" +
                        "Cumplir un año menos\n" +
                        "Europa VII\n" +
                        "La visita\n" +
                        "Sola\n" +
                        "Palabras para Paula\n" +
                        "Flores en la orilla\n" +
                        "Un cuento sobre el agua\n" +
                        "La primera versión\n" +
                        "Veinte penas\n" +
                        "Pequeños momentos";
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(INFO_TITLE, infoTitle);
                intent.putExtra(INFO_IMG, infoImage);
                intent.putExtra(INFO_YEAR, infoYear);
                intent.putExtra(INFO_TIME, infoTime);
                intent.putExtra(INFO_SONG, infoSong);
                startActivity(intent);
            }
        });
        cardView32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String infoTitle = getString(R.string.textDisc6);
                int infoImage = R.drawable.disc6;
                String infoYear = "2011";
                String infoTime = "38:47";
                String infoSong = "La niña que llora en tus fiestas\n" +
                        "Día cero\n" +
                        "Paloma blanca\n" +
                        "Cometas por el cielo\n" +
                        "Las noches que no mueren\n" +
                        "El tiempo a solas\n" +
                        "Promesas de primavera\n" +
                        "Un minuto más\n" +
                        "Mi calle es Nueva York\n" +
                        "Mientras quede por decir una palabra\n" +
                        "Esta vez no digas nada";
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(INFO_TITLE, infoTitle);
                intent.putExtra(INFO_IMG, infoImage);
                intent.putExtra(INFO_YEAR, infoYear);
                intent.putExtra(INFO_TIME, infoTime);
                intent.putExtra(INFO_SONG, infoSong);
                startActivity(intent);
            }
        });
        cardView41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String infoTitle = getString(R.string.textDisc7);
                int infoImage = R.drawable.disc7;
                String infoYear = "2016";
                String infoTime = "48:00";
                String infoSong = "Estoy contigo\n" +
                        "Diciembre\n" +
                        "Verano\n" +
                        "Esa chica\n" +
                        "Pálida Luna\n" +
                        "Camino de tu corazón\n" +
                        "Intocables\n" +
                        "No vales más que yo\n" +
                        "Cuando menos lo merezca\n" +
                        "Mi pequeño gran valiente\n" +
                        "Siempre\n" +
                        "Tan guapa";
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(INFO_TITLE, infoTitle);
                intent.putExtra(INFO_IMG, infoImage);
                intent.putExtra(INFO_YEAR, infoYear);
                intent.putExtra(INFO_TIME, infoTime);
                intent.putExtra(INFO_SONG, infoSong);
                startActivity(intent);
            }
        });
        cardView42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String infoTitle = getString(R.string.textDisc8);
                int infoImage = R.drawable.disc8;
                String infoYear = "2021";
                String infoTime = "39:00";
                String infoSong = "Doblar y Comprender\n" +
                        "Como un Par de Girasoles\n" +
                        "Abrázame\n" +
                        "Durante Una Mirada\n" +
                        "Galerna\n" +
                        "Te Pareces Tanto a Mí\n" +
                        "Menos Tú\n" +
                        "Sirenas\n" +
                        "Acantilado\n" +
                        "Me Voy de Fiesta\n" +
                        "Lo Ves";
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra(INFO_TITLE, infoTitle);
                intent.putExtra(INFO_IMG, infoImage);
                intent.putExtra(INFO_YEAR, infoYear);
                intent.putExtra(INFO_TIME, infoTime);
                intent.putExtra(INFO_SONG, infoSong);
                startActivity(intent);
            }
        });




    }
}